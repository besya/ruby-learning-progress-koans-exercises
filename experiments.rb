
module Enumerable
  def ref(selector, aggregator)
    self.select(&selector).map(&aggregator)
  end
end

nums = [1,2,3,4,5,6,7,8,9]
even_predicate = ->(num) { num % 2 == 0 }
multiplier = ->(num) { num * 2 }

p nums.ref(even_predicate, multiplier)
p nums.ref(->(num) { num % 2 > 0 }, ->(num) { num * 3 })

class Cache
  class << self
    attr_reader :cache
    def instance
      @cache ||= {}
      self
    end
    def get(key)
      @cache[key]
    end
    def set(key, value)
      @cache[key] = value
    end
  end
end

module Cacheable
  def self.included(klass)
    klass.instance_methods(false).each do |method_name|
      old_method = klass.instance_method(method_name)

      proc = proc do |*args|
        cached_result = Cache.instance.get(method_name)
        return cached_result unless cached_result.nil?
        Cache.instance.set(method_name, old_method.bind(self).call(*args))
        Cache.instance.get(method_name)
      end

      klass.instance_eval do
        define_method(method_name, proc)
      end

    end
  end
end

class ShouldBeCached

  def caching_method(num)
    num + 1
  end

  private
  def should_not_cached_method(num)
    num + 10
  end
end

class ShouldBeCached; include Cacheable end

item1 = ShouldBeCached.new
p item1.caching_method 2
p item1.caching_method 3
item2 = ShouldBeCached.new
p item2.caching_method 4

p Cache.cache